# autohooks-plugin-insert-license

An [autohooks](https://github.com/greenbone/autohooks) plugin to insert license in files
via [pre-commit-hooks/insert license](https://github.com/Lucas-C/pre-commit-hooks).

## Installation

### Install using pip

You can install the latest stable release of autohooks-plugin-insert-license from the
Python Package Index using [pip](https://pip.pypa.io/):

    pip install autohooks-plugin-insert-license

Note the `pip` refers to the Python 3 package manager. In an environment where
Python 2 is also available the correct command may be `pip3`.

### Install using poetry

It is highly encouraged to use [poetry](https://python-poetry.org) for
maintaining your project's dependencies. Normally autohooks-plugin-insert-license is
installed as a development dependency.

    poetry install

## Usage

To activate the insert_license autohooks plugin please add the following setting to your
*pyproject.toml* file.

```toml
[tool.autohooks]
pre-commit = ["autohooks.plugins.insert_license"]
```

By default, autohooks plugin insert_license checks all files with a *.py* ending. If
only files in a sub-directory or files with different endings should be
formatted, just add the following setting:

```toml
[tool.autohooks]
pre-commit = ["autohooks.plugins.insert_license"]

[tool.autohooks.plugins.insert_license]
include = ['foo/*.py', '*.foo']
```

By default, autohooks plugin insert_license executes insert_license without any arguments.
But you have to specify the path to the license header file to insert.
To change specific settings, the following plugin configuration can be used:

```toml
[tool.autohooks]
pre-commit = ["autohooks.plugins.insert_license"]

[tool.autohooks.plugins.insert_license]
arguments = ["--license-filepath=/path/to/license_header.txt"]
```

## Maintainer

This project is maintained by [Vincent Texier](https://gitlab.com/VictorIndiaTango).

## Contributing

Your contributions are highly appreciated. Please
[create a merge request](https://gitlab.com/VictorIndiaTango/autohooks-plugin-insert-license/-/merge_requests)
on GitHub. Bigger changes need to be discussed with the development team via the
[issues section at Gitlab](https://gitlab.com/VictorIndiaTango/autohooks-plugin-insert-license/-/issues)
first.

## License

Copyright (C) 2021 [Vincent Texier](https://gitlab.com/VictorIndiaTango).

Licensed under the [GNU General Public License v3.0 or later](LICENSE).
